/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    self: this,
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
	if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|IEMobile)/)) {
            document.addEventListener('deviceready', this.onDeviceReady, false);
	} else {
	    document.onreadystatechange = function () {
		if (document.readyState == "interactive") {
		    app.onDeviceReady();
		}
	    }
	}
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
	window.runTests = new testCode();
    }
};

function testCode() {
    var numPages = 2;
    var numCanvasesPerPage = 60;

    var self = this;
    var pixelWidth = innerWidth * devicePixelRatio;
    var pixelHeight = innerHeight * devicePixelRatio;
    var scale = 1 / devicePixelRatio;
    this.transitionInProcess = false;
    if ((typeof device !== 'undefined') && (device.platform === "Android")) {
	var imgPath = "/android_asset/www/img/";
    } else {
	var imgPath = "img/"
    }
	
    /*
    this.drawRandomLine = function() {
	if (Math.random() < -.001) {
	    self.ctx.clearRect(0,0,self.width, self.height);
	}
	self.ctx.beginPath();
	self.ctx.moveTo(Math.random()*self.width,Math.random()*self.height);
	self.ctx.lineTo(Math.random()*self.width,Math.random()*self.height);
	self.ctx.closePath();
	self.ctx.lineWidth = Math.random()*10;
	self.ctx.strokeStyle = '#'+Math.floor(Math.random()*16777215).toString(16);
	self.ctx.stroke();
	if (Math.random() < .9) {
	    self.drawRandomLine();
	} else {
	    setTimeout(self.drawRandomLine, Math.random()*10);
	}
    }
    */

    this.setKeyframe = function(keyframe) {
	//console.log("keyframe: ",keyframe);
	var s = document.createElement( 'style' );
	s.innerHTML = keyframe;
	document.getElementsByTagName( 'head' )[ 0 ].appendChild( s );
    }

   this.screenEnd = function(screen, e) {
       var placement = (screen === 'onScreen' ? 0 : -innerWidth);
       var screenObj = self[screen];
       screenObj.removeEventListener("webkitAnimationEnd", self[screen+'End'], false);
       screenObj.removeEventListener("webkitTransitionEnd", self[screen+'End'], false); // cover the bases
       screenObj.style.webkitTransitionDuration = "0ms";
       screenObj.style.webkitTransform = 'translate('+placement+'px,0)';
       screenObj.style.opacity = '1';
       screenObj.style.webkitAnimation = '';
       self.transitionInProcess = false;
       if (self.transitionCallback) {
	   (self.transitionCallback)();
	   delete self.transitionCallback;
       }
   };

    this.onScreenEnd = function(e) {
	self.screenEnd('onScreen', e);
    };

    this.offScreenEnd = function(e) {
	self.screenEnd('offScreen', e);
    };

    this.slide = function(direction, shiftOnScreen, shiftOffScreen, speed, callback) {
	if (!self.transitionInProcess) {
	    if (callback) {
		this.transitionCallback = callback;
	    }
	    self.onScreen = shiftOnScreen;
	    self.offScreen = shiftOffScreen;
	    this.transitionInProcess = true;
	    
	    switch (direction) {
	    case 'left':
		shiftOffScreen.style.webkitTransform = 'translate(0px,0)';
		shiftOffScreen.style.webkitAnimation = 'offScreenLeft '+speed+'ms 1 linear forwards';
		shiftOnScreen.style.webkitTransform = 'translate('+innerWidth+'px,0)';
		shiftOnScreen.style.display="block";
		shiftOnScreen.style.webkitAnimation = 'onScreen '+speed+'ms 1 linear forwards';
		break;
	    case 'right':
		shiftOffScreen.style.webkitTransform = 'translate(0px,0)';
		shiftOffScreen.style.webkitAnimation = 'offScreenRight '+speed+'ms 1 linear forwards';
		shiftOnScreen.style.webkitTransform = 'translate(-'+innerWidth+'px,0)';
		shiftOnScreen.style.display="block";
		shiftOnScreen.style.webkitAnimation = 'onScreen '+speed+'ms 1 linear forwards';
		break;
	    default:
		console.error("***No method for page transition");
	    }
	    shiftOnScreen.addEventListener("webkitAnimationEnd", self.onScreenEnd, false);
	    shiftOffScreen.addEventListener("webkitAnimationEnd",self.offScreenEnd, false);
	}
    };

    this.createPage = function(name) {
	var page = document.createElement('div');
	page.style.width = innerWidth;
	page.style.height = innerHeight;
	page.id = name;
	document.body.appendChild(page);
	return page;
    }

    this.createCanvas = function(name, container,left,top,width, height) {
	var canvas = document.createElement('canvas');
	var ctx = canvas.getContext("2d");
	container.appendChild(canvas)
	canvas.id = name;
	canvas.style.position = "absolute";
	canvas.style.left = left +"px";
	canvas.style.top = top + "px";
	canvas.width = width * devicePixelRatio;
	canvas.height = height * devicePixelRatio;
	canvas.style.width = width + "px";
	canvas.style.height = height + "px";
	ctx.scale(devicePixelRatio, devicePixelRatio);
	return [canvas, ctx];
    };

    this.loadImageinCanvas = function(img, ctx, width, height, scale) {
	var imgElem = document.createElement("img");
	imgElem.setAttribute("src",imgPath+img);
	imgElem.onload = (function(ctx,img,width,height) {
	    return function() {
		ctx.drawImage(img,0,0,width,height);
	    };
	})(ctx,imgElem,width*scale,height*scale);
    };

/*
    this.addClickableRegion = function(name, container,x,y, sizeX, sizeY) {
    };
*/
    var pages = [];
    var canvases = [];
    var ctxs = [];
    var imgWidth = 170;
    var imgHeight = 200;
    var imgName = "logo.png";

    this.setKeyframe("@-webkit-keyframes offScreenLeft { 100% { -webkit-transform: translate(-"+innerWidth+"px,0); } }");
    this.setKeyframe("@-webkit-keyframes offScreenRight { 100% { -webkit-transform: translate("+innerWidth+"px,0); } }");

    for (var i = 0; i < numPages; i++) {
	pages[i] = this.createPage('page'+i);
	for (var j = 0; j < numCanvasesPerPage; j++) {
	    var randomXPos = Math.random() * (innerWidth - imgWidth * scale);
	    var randomYPos = Math.random() * (innerHeight - imgHeight * scale);
	    var canvasNum = i * numCanvasesPerPage + j;
	    var  canvasInfo =  this.createCanvas("canvas"+canvasNum, pages[i],randomXPos,randomYPos,imgWidth, imgHeight);
	    canvases[canvasNum] = canvasInfo[0];
	    ctxs[canvasNum] = canvasInfo[1];
	    this.loadImageinCanvas(imgName, ctxs[canvasNum], imgWidth, imgHeight, scale);
	}
	pages[i].style.webkitTransform = 'translate('+innerWidth+'px,0)';
    }
    pages[0].style.webkitTransform = 'translate(0px,0)';
    setInterval(function() {
	self.slide('left', pages[1], pages[0], 1000);
	setTimeout(function() {
	    self.slide('right', pages[0], pages[1], 1000);
	}, 2500)
    },5000);
    //setTimeout(self.drawRandomLine, Math.random()*10);
    
        //this.receivedEvent('deviceready');
    
}